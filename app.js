var fs      = require('fs'),
    express = require('express'),
    events  = require('events');


// global object

global.hallOfFame = Object.create({}, {
    "emitter" : {
        value       : new events.EventEmitter(),
        writable    : false
    },
    "config" : {
        value       : JSON.parse(fs.readFileSync('config.json', 'utf8')),
        writable    : false
    },
    "app" : {
        value       : express(),
        writable    : false
    }
});

hallOfFame.emitter.setMaxListeners(0);

hallOfFame.app.set('trust proxy');
hallOfFame.app.disable('x-powered-by');


// middlewares

hallOfFame.app.use(express.static('public'));


// routes

require('./app/node/routes')();


// error handling

hallOfFame.app.use(function(req, res){
    res.type('text/plain');
    res.status(404);
    res.send('404 - Not Found');
});


hallOfFame.app.use(function(err, req, res){
    console.error(err.stack);
    res.type('text/plain'); 
    res.status(500);
    res.send('500 - Server Error');
});


// init

hallOfFame.app.listen(hallOfFame.config.port);