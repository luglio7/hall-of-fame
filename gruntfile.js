module.exports = function(grunt){
    var defaultTask;

    grunt.initConfig({
        pkg : grunt.file.readJSON('package.json'),

        browserify : {
            frontJs : {
                files : {
                    'public/js/app.js' : 'app/front/main.js'
                }
            }
        },

        jshint : {
            options : {
                curly   : true,
                eqeqeq  : true,
                undef   : true,
                unused  : true,
                node    : true,
                devel   : true
            },
            frontJs : {
                options : {
                    browser : true,
                    strict  : true,
                    jquery  : true
                },
                src : ['app/front/**/*.js']
            },
            nodeJs : {
                options : {
                    globals : {
                        'hallOfFame' : true
                    }
                },
                src : ['app/node/**/*.js', 'app.js', 'gruntfile.js']
            }
        },

        stylus : {
            css : {
                options : {
                    compress : false,
                    paths    : ['public/stylus/']
                },
                src  : 'public/stylus/style.styl',
                dest : 'public/css/style.css'
            }
        },

        watch : {
            options : {
                spawn : false
            },
            frontJs : {
                files : 'app/front/**/*.js',
                tasks : ['js-front']
            },
            nodeJs : {
                files : ['app/node/**/*.js', 'app.js', 'gruntfile.js'],
                tasks : ['js-node']
            },
            stylus : {
                files : 'public/stylus/*.styl',
                tasks : ['stylus']
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-browserify');

    defaultTask = ['jshint:frontJs', 'browserify:frontJs'];

    grunt.registerTask('default',       defaultTask);
    grunt.registerTask('js-front',      defaultTask);
    grunt.registerTask('js-node',       ['jshint:nodeJs']);

};