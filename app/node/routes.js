var fs = require('fs'),
    hb = require('handlebars');

module.exports = function(){ 

    // ROOT ROUTE

    hallOfFame.app.get('/', function(req, res){
        var source       = fs.readFileSync('view/index.hbs', { encoding : 'utf8' }),
            template     = hb.compile(source),
            templateData = hallOfFame.config.view;

        templateData.logo = fs.readFileSync('public/svg/logo.svg', { encoding : 'utf8' });
        
        res.type('text/html');
        res.send(template(templateData));
    });
};